#!/bin/bash

echo "run django"

python manage.py migrate
python manage.py collectstatic --no-input

# gunicorn start
# -----------------------------------------------------------------------------

gunicorn config.wsgi:application \
  --workers 3 \
  --timeout 60 \
  --keep-alive 5 \
  --bind 0.0.0.0:8000 \
  --chdir=/project \
  --log-level=info \
  --log-file=-
