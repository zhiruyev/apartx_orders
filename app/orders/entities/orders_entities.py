from uuid import uuid4

from dataclasses import dataclass


@dataclass
class OrderResponseInputEntity:
    id: uuid4


@dataclass
class MyOrdersInputEntity:
    user_email: str


@dataclass
class MyOrderResponsesInputEntity:
    user_email: str


@dataclass
class FinishOrderEmployeeInputEntity:
    user_email: str


@dataclass
class FinishOrderOwnerOutputEntity:
    user_email: str


@dataclass
class EstimateInputEntity:
    square: str
