import uuid

from django.db import models
from django.utils.translation import gettext as _
from django_extensions.db.models import TimeStampedModel

from app.libs.base_models import NamedModel
from app.orders.constant import FileType
from app.orders.utils.file_upload import (
    file_orders_upload,
    file_finished_order_upload,
)


class Tag(NamedModel):
    uuid = models.UUIDField(default=uuid.uuid4, primary_key=True, editable=False)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("Тэг")
        verbose_name_plural = _("Тэги")


class OrderFile(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, primary_key=True, editable=False)
    type = models.PositiveSmallIntegerField(choices=FileType.choices(), default=FileType.TYPE_IMAGE)
    file = models.FileField(upload_to=file_orders_upload)

    class Meta:
        verbose_name = _("Файл заказа")
        verbose_name_plural = _("Файлы заказов")


class CheckListItem(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, primary_key=True, editable=False)
    description = models.CharField(max_length=100)
    is_worker_done = models.BooleanField(default=False)

    class Meta:
        verbose_name = _("Чек лист")
        verbose_name_plural = _("Чек листы")


class Order(TimeStampedModel):
    uuid = models.UUIDField(default=uuid.uuid4, primary_key=True, editable=False)
    user_email = models.CharField(max_length=200)

    title = models.CharField(max_length=100, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    price = models.FloatField(null=True, blank=True)
    square_total = models.FloatField(null=True, blank=True)

    is_employee_selected = models.BooleanField(null=True, blank=True, default=False)
    is_employee_finished = models.BooleanField(null=True, blank=True, default=False)
    is_owner_accepted = models.BooleanField(null=True, blank=True, default=False)

    tags = models.ManyToManyField(Tag, null=True, blank=True)
    files = models.ManyToManyField(OrderFile, null=True, blank=True)
    check_list_items = models.ManyToManyField(CheckListItem, null=True, blank=True)

    def employee_selected(self):
        self.is_employee_selected = True
        self.save(update_fields=['is_employee_selected'])

    def employee_finished(self):
        self.is_employee_finished = True
        self.save(update_fields=['is_employee_finished'])

    def owner_accepted(self):
        self.is_owner_accepted = True
        self.save(update_fields=['is_owner_accepted'])

    @property
    def responses(self):
        return OrderResponse.objects.filter(order=self)

    @property
    def reports(self):
        return FinishedOrderReport.objects.filter(order=self)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _("Заказ")
        verbose_name_plural = _("Заказы")


class OrderResponse(TimeStampedModel):
    uuid = models.UUIDField(default=uuid.uuid4, primary_key=True, editable=False)
    order = models.ForeignKey(Order, null=True, blank=True, on_delete=models.SET_NULL)

    user_email = models.CharField(max_length=200)
    text = models.TextField(null=True, blank=True)
    hours = models.IntegerField(default=0)
    is_selected = models.BooleanField(default=False)
    
    def selected(self):
        self.is_selected = True
        self.save(update_fields=['is_selected'])

    def __str__(self):
        return self.text

    class Meta:
        verbose_name = _("Ответ на заказ")
        verbose_name_plural = _("Ответы на заказы")


class FinishedOrderReportFile(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, primary_key=True, editable=False)
    type = models.PositiveSmallIntegerField(choices=FileType.choices(), default=FileType.TYPE_IMAGE)
    file = models.FileField(upload_to=file_finished_order_upload)

    class Meta:
        verbose_name = _("Файл отчета заказа")
        verbose_name_plural = _("Файлы отчетов заказов")


class FinishedOrderReport(TimeStampedModel):
    uuid = models.UUIDField(default=uuid.uuid4, primary_key=True, editable=False)
    order = models.ForeignKey(Order, null=True, blank=True, on_delete=models.SET_NULL)
    user_email = models.CharField(max_length=200)

    files = models.ManyToManyField(FinishedOrderReportFile, null=True, blank=True)

    def __str__(self):
        return self.user_email

    class Meta:
        verbose_name = _("Отчет о выполнении")
        verbose_name_plural = _("Отчеты о выполненных работах")
