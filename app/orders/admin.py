from django.contrib import admin

from app.orders.models import (
    Tag,
    Order,
    OrderResponse,
    FinishedOrderReport,
)


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    search_fields = ['name']


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    search_fields = ['title', 'user_email']


@admin.register(OrderResponse)
class OrderResponseAdmin(admin.ModelAdmin):
    search_fields = ['user_email']
    list_filter = ['is_selected',]


@admin.register(FinishedOrderReport)
class FinishedOrderReportAdmin(admin.ModelAdmin):
    search_fields = ['user_email']
