from rest_framework.routers import DefaultRouter
from app.orders.views import orders_views

app_name = "orders"

router = DefaultRouter()
router.register("tags", orders_views.TagViewSet, basename="tags")
router.register("orders", orders_views.OrderViewSet, basename="orders")
router.register("order_responses", orders_views.OrderResponseViewSet, basename="order_responses")
router.register("order_files", orders_views.OrderFileViewSet, basename="order_files")
router.register(
    "finished_order_reports",
    orders_views.FinishedOrderReportViewSet,
    basename="finished_order_reports"
)
router.register(
    "finished_order_report_files",
    orders_views.FinishedOrderReportFileViewSet,
    basename="finished_order_report_files"
)
router.register(
    "check_list_items",
    orders_views.CheckListItemViewSet,
    basename="check_list_items"
)
router.register(
    "estimate_price",
    orders_views.EstimatePriceViewSet,
    basename="estimate_price"
)

urlpatterns = router.get_urls()
