from rest_framework import serializers

from app.libs.serialziers import BaseSerializer
from app.orders.entities.orders_entities import (
    OrderResponseInputEntity,
    FinishOrderOwnerOutputEntity, EstimateInputEntity,
)
from app.orders.models import (
    Order,
    Tag,
    OrderFile,
    OrderResponse,
    FinishedOrderReportFile,
    FinishedOrderReport,
    CheckListItem,
)

from django.contrib.auth import get_user_model

User = get_user_model()


class TagListSerializer(serializers.ModelSerializer):

    class Meta:
        model = Tag
        fields = [
            'uuid',
            'name',
        ]


class OrderFileCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = OrderFile
        fields = [
            'uuid',
            'type',
            'file'
        ]

        read_only_fields = ['uuid']


class FinishedOrderReportFileCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = FinishedOrderReportFile
        fields = [
            'uuid',
            'type',
            'file'
        ]

        read_only_fields = ['uuid']


class FinishedOrderReportCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = FinishedOrderReport
        fields = [
            'uuid',
            'user_email',
            'files',
            'order',
        ]

        extra_kwargs = {
            "user_email": {"required": False},
        }


class FinishedOrderReportFileListSerializer(serializers.ModelSerializer):

    class Meta:
        model = FinishedOrderReportFile
        fields = [
            'uuid',
            'type',
            'file'
        ]


class FinishedOrderReportListSerializer(serializers.ModelSerializer):
    files = FinishedOrderReportFileListSerializer(many=True)

    class Meta:
        model = FinishedOrderReport
        fields = [
            'uuid',
            'user_email',
            'files',
            'order',
        ]


class OrderFileListSerializer(serializers.ModelSerializer):

    class Meta:
        model = OrderFile
        fields = [
            'uuid',
            'type',
            'file'
        ]


class CheckListItemListSerializer(serializers.ModelSerializer):

    class Meta:
        model = CheckListItem
        fields = [
            'uuid',
            'description',
            'is_worker_done',
        ]


class OrderListSerializer(serializers.ModelSerializer):
    reports = FinishedOrderReportListSerializer(read_only=True, many=True)
    tags = TagListSerializer(read_only=True, many=True)
    check_list_items = CheckListItemListSerializer(read_only=True, many=True)

    class Meta:
        model = Order
        fields = [
            'uuid',
            'title',
            'square_total',
            'tags',
            'price',
            'check_list_items',
            'is_employee_selected',
            'reports',
        ]


class OrderCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Order
        fields = [
            'uuid',
            'user_email',
            'title',
            'description',
            'price',
            'square_total',
            'tags',
            'files',
            'check_list_items',
        ]

        read_only_fields = ['uuid']
        extra_kwargs = {
            "user_email": {"required": False},
        }


class OrderResponseRetrieveSerializer(serializers.ModelSerializer):

    class Meta:
        model = OrderResponse
        fields = [
            'uuid',
            'user_email',
            'text',
            'hours',
        ]


class OrderForResponseSerializer(serializers.ModelSerializer):

    class Meta:
        model = Order
        fields = [
            'uuid',
            'title',
        ]


class OrderResponseMyListSerializer(serializers.ModelSerializer):
    order = OrderForResponseSerializer()

    class Meta:
        model = OrderResponse
        fields = [
            'uuid',
            'order',
            'user_email',
            'text',
            'hours',
        ]


class OrderRetrieveSerializer(serializers.ModelSerializer):
    tags = TagListSerializer(many=True)
    files = OrderFileListSerializer(many=True)
    responses = OrderResponseRetrieveSerializer(read_only=True, many=True)

    class Meta:
        model = Order
        fields = [
            'uuid',
            'created',
            'user_email',
            'title',
            'description',
            'price',
            'square_total',
            'tags',
            'files',
            'responses',
        ]


class OrderResponseCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = OrderResponse
        fields = [
            'uuid',
            'user_email',
            'order',
            'text',
            'hours',
        ]

        read_only_fields = ['uuid']
        extra_kwargs = {
            "user_email": {"required": False},
        }


class ChooseOrderResponseInputSerializer(BaseSerializer):
    id = serializers.UUIDField()

    def create(self, validated_data):
        return OrderResponseInputEntity(**validated_data)


class FinishOrderOwnerOutputSerializer(BaseSerializer):
    user_email = serializers.CharField(max_length=200)

    def create(self, validated_data):
        return FinishOrderOwnerOutputEntity(**validated_data)


class CheckListItemCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = CheckListItem
        fields = [
            'uuid',
            'description',
        ]

        read_only_fields = ['uuid']


class EntimateInputSerializer(BaseSerializer):
    square = serializers.FloatField()

    def create(self, validated_data):
        return EstimateInputEntity(**validated_data)
