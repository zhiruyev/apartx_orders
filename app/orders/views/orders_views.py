import pickle

from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from app.libs.auth import (
    IsCustomerApartxClient,
    ApartxAuthentication,
)
from app.libs.serialziers import NoneSerializer
from app.orders.entities.orders_entities import (
    MyOrderResponsesInputEntity,
    MyOrdersInputEntity,
    FinishOrderEmployeeInputEntity,
)
from app.orders.handlers.orders_handler import OrdersHandler, OrderResponsesHandler

from app.orders.models import (
    Order,
    Tag,
    OrderFile,
    OrderResponse,
    FinishedOrderReportFile,
    FinishedOrderReport,
    CheckListItem,
)
from app.orders.serializers.orders_serializers import (
    OrderListSerializer,
    TagListSerializer,
    OrderCreateSerializer,
    OrderFileCreateSerializer,
    OrderRetrieveSerializer,
    OrderResponseCreateSerializer,
    ChooseOrderResponseInputSerializer,
    OrderResponseMyListSerializer,
    FinishedOrderReportFileCreateSerializer,
    FinishedOrderReportCreateSerializer,
    FinishOrderOwnerOutputSerializer,
    CheckListItemCreateSerializer, EntimateInputSerializer,
)


class OrderFileViewSet(
    generics.CreateAPIView,
    GenericViewSet,
):
    queryset = OrderFile.objects.all()

    def get_serializer_class(self):
        return {
            "create": OrderFileCreateSerializer,
        }[self.action]


class FinishedOrderReportFileViewSet(
    generics.CreateAPIView,
    GenericViewSet,
):
    queryset = FinishedOrderReportFile.objects.all()

    def get_serializer_class(self):
        return {
            "create": FinishedOrderReportFileCreateSerializer,
        }[self.action]


class FinishedOrderReportViewSet(
    generics.CreateAPIView,
    GenericViewSet,
):
    queryset = FinishedOrderReport.objects.all()
    authentication_classes = (ApartxAuthentication,)

    def get_serializer_class(self):
        return {
            "create": FinishedOrderReportCreateSerializer,
        }[self.action]

    def perform_create(self, serializer):
        serializer.save(user_email=self.request.user.username)
        order = serializer.validated_data['order']
        order.employee_finished()


class TagViewSet(
    generics.ListAPIView,
    GenericViewSet,
):
    queryset = Tag.objects.all()

    def get_serializer_class(self):
        return {
            "list": TagListSerializer,
        }[self.action]


class OrderViewSet(
    generics.ListAPIView,
    generics.CreateAPIView,
    generics.RetrieveAPIView,
    generics.DestroyAPIView,
    GenericViewSet,
):
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['tags',]
    authentication_classes = (ApartxAuthentication,)

    def get_queryset(self):
        if self.action == 'list':
            return Order.objects.filter(is_employee_selected=False)

        return Order.objects.all()

    def get_serializer_class(self):
        return {
            "list": OrderListSerializer,
            "create": OrderCreateSerializer,
            "retrieve": OrderRetrieveSerializer,
            "choose_order_response": ChooseOrderResponseInputSerializer,
            "delete": NoneSerializer,
            "my": OrderListSerializer,
            "my_not_done": OrderListSerializer,
            "my_in_progress": OrderListSerializer,
            "my_on_review": OrderListSerializer,
            "finish_order_owner": FinishOrderOwnerOutputSerializer,
        }[self.action]

    def perform_create(self, serializer):
        serializer.save(user_email=self.request.user.username)

    @action(methods=['post'], detail=True)
    def finish_order_employee(self, request, pk, *args, **kwargs):
        order = self.get_object()
        input_entity = FinishOrderEmployeeInputEntity(user_email=self.request.user.username)

        OrdersHandler(order=order).finish_order_employee(
            input_entity=input_entity
        )

        return Response()

    @action(methods=['get'], detail=True)
    def finish_order_owner(self, request, pk, *args, **kwargs):
        order = self.get_object()

        response = OrdersHandler(order=order).finish_order_owner()
        serializer = self.get_serializer(response)

        return Response(serializer.data)

    @action(methods=['post'], detail=True)
    def choose_order_response(self, request, pk, *args, **kwargs):
        order = self.get_object()

        serializer = self.get_serializer(data=request.data)

        serializer.is_valid(raise_exception=True)
        input_entity = serializer.save()

        OrdersHandler(order=order).choose_order_response(
            input_entity=input_entity
        )

        return Response()

    @action(methods=['get'], detail=False)
    def my(self, request, *args, **kwargs):
        input_entity = MyOrdersInputEntity(user_email=self.request.user.username)

        orders = OrdersHandler().my(
            input_entity=input_entity
        )
        serializer = self.get_serializer(orders, many=True)

        return Response(serializer.data)

    @action(methods=['get'], detail=False)
    def my_not_done(self, request, *args, **kwargs):
        input_entity = MyOrdersInputEntity(user_email=self.request.user.username)

        orders = OrdersHandler().my_not_done(
            input_entity=input_entity
        )
        serializer = self.get_serializer(orders, many=True)

        return Response(serializer.data)

    @action(methods=['get'], detail=False)
    def my_in_progress(self, request, *args, **kwargs):
        input_entity = MyOrdersInputEntity(user_email=self.request.user.username)

        orders = OrdersHandler().my_in_progress(
            input_entity=input_entity
        )
        serializer = self.get_serializer(orders, many=True)

        return Response(serializer.data)

    @action(methods=['get'], detail=False)
    def my_on_review(self, request, *args, **kwargs):
        input_entity = MyOrdersInputEntity(user_email=self.request.user.username)

        orders = OrdersHandler().my_on_review(
            input_entity=input_entity
        )
        serializer = self.get_serializer(orders, many=True)

        return Response(serializer.data)


class OrderResponseViewSet(
    generics.CreateAPIView,
    generics.ListAPIView,
    generics.UpdateAPIView,
    generics.DestroyAPIView,
    GenericViewSet,
):
    authentication_classes = (ApartxAuthentication,)
    queryset = OrderResponse.objects.all()

    def get_serializer_class(self):
        return {
            "create": OrderResponseCreateSerializer,
            "delete": NoneSerializer,
            "my": OrderResponseMyListSerializer,
        }[self.action]

    def perform_create(self, serializer):
        serializer.save(user_email=self.request.user.username)

    @action(methods=['get'], detail=False)
    def my(self, request, *args, **kwargs):
        input_entity = MyOrderResponsesInputEntity(user_email=self.request.user.username)

        order_responses = OrderResponsesHandler().my(
            input_entity=input_entity
        )
        serializer = self.get_serializer(order_responses, many=True)

        return Response(serializer.data)


class CheckListItemViewSet(
    generics.CreateAPIView,
    GenericViewSet,
):
    queryset = CheckListItem.objects.all()

    def get_serializer_class(self):
        return {
            "create": CheckListItemCreateSerializer,
        }[self.action]

    @action(methods=['post'], detail=True)
    def mark_as_done(self, request, *args, **kwargs):
        item = self.get_object()

        item.is_worker_done = True
        item.save()

        return Response()


class EstimatePriceViewSet(
    generics.CreateAPIView,
    GenericViewSet,
):
    def get_serializer_class(self):
        return {
            "estimate": EntimateInputSerializer,
        }[self.action]

    @action(methods=['get'], detail=False)
    def estimate(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.query_params)

        serializer.is_valid(raise_exception=True)
        input_entity = serializer.save()

        loaded_model = pickle.load(open('app/orders/pickle_models/finalized_model.sav', 'rb'))
        result = loaded_model.predict([[input_entity.square]])

        return Response({
            'assesed_price': result[0][0]
        })
