# Generated by Django 4.0.6 on 2023-06-24 20:16

import app.orders.constant
import app.orders.utils.file_upload
from django.db import migrations, models
import django.db.models.deletion
import django_extensions.db.fields
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0006_order_is_employee_finished_order_is_owner_accepted'),
    ]

    operations = [
        migrations.CreateModel(
            name='FinishedOrderReportFile',
            fields=[
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('type', models.PositiveSmallIntegerField(choices=[(1, 'TYPE_IMAGE')], default=app.orders.constant.FileType['TYPE_IMAGE'])),
                ('file', models.FileField(upload_to=app.orders.utils.file_upload.file_finished_order_upload)),
            ],
            options={
                'verbose_name': 'Файл отчета заказа',
                'verbose_name_plural': 'Файлы отчетов заказов',
            },
        ),
        migrations.CreateModel(
            name='FinishedOrderReport',
            fields=[
                ('created', django_extensions.db.fields.CreationDateTimeField(auto_now_add=True, verbose_name='created')),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(auto_now=True, verbose_name='modified')),
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('user_email', models.CharField(max_length=200)),
                ('files', models.ManyToManyField(blank=True, null=True, to='orders.orderfile')),
                ('order', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='orders.order')),
            ],
            options={
                'verbose_name': 'Ответ на заказ',
                'verbose_name_plural': 'Ответы на заказы',
            },
        ),
    ]
