from app.libs.randomize import get_random_string


def file_orders_upload(instance, filename):
    y = get_random_string(25)
    extension = filename.split(".")[-1]
    return f"orders/order_file/{y}.{extension}"


def file_finished_order_upload(instance, filename):
    y = get_random_string(25)
    extension = filename.split(".")[-1]
    return f"orders/finished_order_report_file/{y}.{extension}"
