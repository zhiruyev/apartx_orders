from typing import List

from app.orders.entities.orders_entities import (
    OrderResponseInputEntity,
    MyOrderResponsesInputEntity,
    MyOrdersInputEntity,
    FinishOrderEmployeeInputEntity,
    FinishOrderOwnerOutputEntity,
)
from app.orders.models import (
    Order,
    OrderResponse,
)

from django.contrib.auth import get_user_model
from rest_framework.exceptions import PermissionDenied

User = get_user_model()


class OrdersHandler:

    def __init__(self, order: Order = None):
        self.order = order

    def choose_order_response(
        self,
        input_entity: OrderResponseInputEntity
    ) -> None:
        self.order.employee_selected()
        order_response: OrderResponse = OrderResponse.objects.get(uuid=input_entity.id)
        order_response.selected()

    def my(
        self,
        input_entity: MyOrdersInputEntity
    ) -> List[Order]:
        return Order.objects.filter(user_email=input_entity.user_email)

    def my_not_done(
        self,
        input_entity: MyOrdersInputEntity
    ) -> List[Order]:
        responses = OrderResponse.objects.filter(
            user_email=input_entity.user_email,
            is_selected=True,
        )
        not_done_orders = []
        for resp in responses:
            if not resp.order.is_employee_finished:
                not_done_orders.append(resp.order)

        return not_done_orders

    def my_in_progress(
        self,
        input_entity: MyOrdersInputEntity
    ) -> List[Order]:
        responses = Order.objects.filter(
            user_email=input_entity.user_email,
            is_employee_selected=True,
            is_employee_finished=False,
        )

        return responses

    def my_on_review(
        self,
        input_entity: MyOrdersInputEntity
    ) -> List[Order]:
        responses = Order.objects.filter(
            user_email=input_entity.user_email,
            is_employee_selected=True,
            is_employee_finished=True,
            is_owner_accepted=False,
        )

        return responses

    def finish_order_employee(
        self,
        input_entity: FinishOrderEmployeeInputEntity
    ) -> None:
        order_response = OrderResponse.objects.filter(
            order=self.order,
            user_email=input_entity.user_email,
            is_selected=True
        )
        if not order_response.exists():
            raise PermissionDenied("Not selected response")

        self.order.employee_finished()

    def finish_order_owner(
        self
    ) -> FinishOrderOwnerOutputEntity:
        self.order.owner_accepted()
        response = OrderResponse.objects.get(order=self.order)

        return FinishOrderOwnerOutputEntity(user_email=response.user_email)


class OrderResponsesHandler:

    def __init__(self, order: Order = None):
        self.order = order

    def my(
        self,
        input_entity: MyOrderResponsesInputEntity
    ) -> List[OrderResponse]:
        return OrderResponse.objects.filter(user_email=input_entity.user_email)
